#!/usr/bin/env python
""" RDM steering interface
# -*- coding: utf-8 -*-
#
# Author : Ahmad Muaz
# Email : amuaz@moovita.com
# Department : eMoovit, MY
# Last modified: 22-JAN-2019
# version comments - copy from RDM interface to work with drive force G920
"""

# to install evdev, $ sudo apt-get install python-evdev --mz

import rospy
import pygame
import evdev # added for device event --mz
from evdev import ecodes as e, InputDevice, ff
import serial
import time
from pprint import pprint
import logging
import os
from ConfigParser import SafeConfigParser
from moovita_message_definitions.msg import wheel_console


FILE_LOCATION = os.path.dirname(os.path.abspath(__file__))

#------------Config Parser---------
config = SafeConfigParser()
config.read(FILE_LOCATION+'/config/drive_force_config.ini')

FORWARD_MAX_SPEED = config.getfloat('WHEEL_CONSOLE', 'FORWARD_MAX_SPEED')
REVERSE_MAX_SPEED = config.getfloat('WHEEL_CONSOLE', 'REVERSE_MAX_SPEED')
SPEED_SCALE = config.getfloat('WHEEL_CONSOLE', 'SPEED_SCALE')
PUBLISH_FREQ = config.getfloat('WHEEL_CONSOLE', 'PUBLISH_FREQ')
STEERING_RANGE = config.getint('WHEEL_CONSOLE', 'STEERING_RANGE')
BRAKE_RANGE = config.getint('WHEEL_CONSOLE', 'BRAKE_RANGE')
GAS_RANGE = config.getint('WHEEL_CONSOLE', 'GAS_RANGE')
LOG_LEVEL = config.get('WHEEL_CONSOLE', 'LOG_LEVEL')



#--------------end of Config -------


class ConnectPythonLoggingToROS(logging.Handler):

    MAP = {
        logging.DEBUG:rospy.logdebug,
        logging.INFO:rospy.loginfo,
        logging.WARNING:rospy.logwarn,
        logging.ERROR:rospy.logerr,
        logging.CRITICAL:rospy.logfatal
    }

    def emit(self, record):
        try:
            self.MAP[record.levelno]("%s: %s" % (record.name, record.msg))
        except KeyError:
            rospy.logerr("unknown log level %s LOG: %s: %s" % (record.levelno, record.name, record.msg))
            

#----------Logging Initalize -------#

#reconnect logging calls which are children of this to the ros log system
logging.getLogger('drive_force').addHandler(ConnectPythonLoggingToROS())

if not os.path.exists("logs"):
    os.makedirs("logs")
    
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("drive_force")

# file handler
handler = logging.FileHandler('logs/drive_force_steering_interface.log')
handler.setLevel(logging.INFO)

# logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

# declare and check for compatibility devices
for name in evdev.list_devices():
    dev = InputDevice(name)
    if e.EV_FF in dev.capabilities():
        logger.debug("devices has FF capabilities")
        break
    else:
        logger.debug("devices has no FF capabilities")
        break


#-------------- limit -----------------#

def limit(value,limiter_value):
    if value > limiter_value:
        value = limiter_value
    elif value < -limiter_value:
        value = -limiter_value
    return value

#-------------- valmap -----------------#

def valmap(value, istart, istop, ostart, ostop):
    result = ostart + (ostop - ostart) * ((value - istart) / (istop - istart))
    return result


#-------------- initialized Force Feedback --------------#
def initEffect():
    _force = 0

    envelope = ff.Envelope(
            attack_length=0, 
            attack_level=(int)(_force*32767.0), 
            fade_length=0, 
            fade_level=(int)(_force*32767.0))

    constant = ff.Constant(
            level=(int)(_force*32767.0), 
            envelope=envelope)

    constant_effect = ff.EffectType(ff_constant_effect=constant)

    duration_ms = 0

    r_effect = ff.Effect(
        e.FF_CONSTANT, -1, 0xC000,
        ff.Trigger(0, 0),
        ff.Replay(duration_ms, 0),
        ff.EffectType(ff_constant_effect=constant)
    )
    repeat_count = 1
    effect_r = dev.upload_effect(r_effect)
    dev.write(e.EV_FF, effect_r, repeat_count)

#------------------ update Force Feedback -----------------#

def updateEffect(force,_time):
    _force = force
    envelope = ff.Envelope(
            attack_length=0, 
            attack_level=(int)(_force*32767.0), 
            fade_length=0, 
            fade_level=(int)(_force*32767.0))

    constant = ff.Constant(
            level=(int)(_force*32767.0), 
            envelope=envelope)

    constant_effect = ff.EffectType(ff_constant_effect=constant)

    duration_ms = _time

    r_effect = ff.Effect(
        e.FF_CONSTANT, -1, 0xC000,
        ff.Trigger(0, 0),
        ff.Replay(duration_ms, 0),
        ff.EffectType(ff_constant_effect=constant)
    )
    repeat_count = 1
    effect_r = dev.upload_effect(r_effect)
    dev.write(e.EV_FF, effect_r, repeat_count)

#----------Logging Initalize -------#

temp_steering = 0
temp_brake = 0
temp_gas = 0
initEffect()

print "\n"
print "Testing and initialize all input devices before continuing"
print "To test steering wheel, please rotate clockwise or counter-clockwise"
print "To test gas, please push throttle pedal"
print "To test brake, please push brake pedal"
print "To test force feedback, please push LSB and RSB at the same time"
print "devices : {}" .format(dev.name)



class DriveForceSteeringInterface:
    def read_joystick(self):
        ''' Main loop to read joystick '''
        while (not self.done) and (not rospy.is_shutdown()):
            
            if not os.path.exists(self.device_path):
                time.sleep(0.5)
                try:
                    logger.info("Reconnecting")
                    # Initialize the joysticks
                    pygame.joystick.quit()
                    pygame.joystick.init()
                    self.joystick_count = pygame.joystick.get_count()
                    logger.info("joystick found : " + str(self.joystick_count))
                    self.init_status = False
                except Exception as e:
                    logger.error(e)
                    pygame.joystick.quit()
                    self.init_status = False
                    time.sleep(1)

            else:
                # EVENT PROCESSING STEP
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        self.done = True
                        self.init_status = False
                    
                    # print "SINI"

                    # Possible joystick actions: JOYAXISMOTION JOYBALLMOTION JOYBUTTONDOWN
                    # JOYBUTTONUP JOYHATMOTION  
                    
                    if event.type == pygame.JOYBUTTONDOWN:
                        # logger.info("Joystick button pressed.")
                        if int(joystick.get_button(9)) == 1 and int(joystick.get_button(8)) == 1 :
                            if self.init_status == False:
                                self.init_status = True
                                print "testing for force feedback"
                                print "please hold steering wheel firmly"
                                time.sleep(1)
                                updateEffect(1,1000)
                                time.sleep(1)
                                dev.erase_effect(1)
                                updateEffect(-1,1000)
                                time.sleep(1)
                                dev.erase_effect(1)
                                logger.info("init status ------- OK")

                    if event.type == pygame.JOYAXISMOTION:
                        
                        if joystick.get_axis(0):
                            if self.init_steering == False:
                                self.init_steering = True
                                logger.info("Steering Wheel ---- OK")
                        
                        if joystick.get_axis(1):
                            if self.init_gas == False:
                                self.init_gas = True
                                logger.info("Gas --------------- OK")
                        
                        if joystick.get_axis(2):
                            if self.init_brake == False:
                                self.init_brake = True    
                                logger.info("Brake ------------- OK")

                # print "INIT >>> brake: {}, steering: {}, gas: {}, status: {}" .format(self.init_brake, self.init_steering, self.init_gas, self.init_status)
            
                joystick = pygame.joystick.Joystick(0)
                joystick.init()
                self.joystick_count = pygame.joystick.get_count()

                if self.init_brake and self.init_gas and self.init_steering and self.init_status == True:
                    if self.init_system == False:
                        self.init_system = True
                        logger.info("all system check and initialize ------ OK")
                        # logger.info("devices {} have been initialize with force feedback and ready to be used"))
                        
                    temp_steering   = joystick.get_axis( 0 )  # get value from axis 2
                    temp_brake   = joystick.get_axis( 2 )
                    temp_gas   = joystick.get_axis( 1 )
                    
                    # print "RAW >>> steering:{}, brake:{}, gas:{}" .format(temp_steering, temp_brake, temp_gas)

                    temp_steering   = valmap(temp_steering, -1.0,  1.0, -100, 100)
                    temp_brake      = valmap(temp_brake,     1.0, -1.0,    0, 100)
                    temp_gas        = valmap(temp_gas,       1.0, -1.0,    0, 100)
                    
                    # print "MAP >>> steering:{}, brake:{}, gas:{}" .format(temp_steering, temp_brake, temp_gas)

                    self.steering   = int(limit(temp_steering,  STEERING_RANGE))
                    self.brake      = int(limit(temp_brake,     BRAKE_RANGE))
                    self.gas        = int(limit(temp_gas,       GAS_RANGE))
                    
                    # print "LIM >>> steering:{}, brake:{}, gas:{}" .format(self.steering, self.brake, self.gas)
                else:
                    logger.debug("all system check and initialize ------ NOT COMPLETE")
                    temp_brake = 0
                    temp_gas = 0
                    temp_steering = 0

                alpha = 10
                force = alpha * (valmap(float(temp_steering),-100,100,1.0,-1.0))

                if force > 1:
                    force = 1
                elif force < -1:
                    force = -1
                
                updateEffect(force,1000)
                # time.sleep(0.01)
                
                _msg = "INIT: %s, GAS: %s, STEERING : %s, ESTOP : %s, JOYSTICKS : %s" %(self.init_status, self.gas, self.steering, self.estop ,  self.joystick_count)
                logger.debug(_msg)

                pub_msg = wheel_console()
                pub_msg.header.stamp = rospy.Time.now()
                pub_msg.header.frame_id = 'moovita'
                
                
                if self.init_status:
                    pub_msg.cmd_steering = self.steering
                    pub_msg.cmd_speed = self.gas

                else:
                    pub_msg.cmd_speed = 0

                pub_msg.is_estop = self.estop
                pub_msg.is_brake = self.is_brake
                pub_msg.is_init = self.init_status

                self.ros_publisher.publish(pub_msg)
                
            time.sleep(PUBLISH_FREQ)
            try:
                dev.erase_effect(1)
            except:
                logger.info("no effect upload yet")
                logger.info("check if dev.upload_effect(effect_id) has been called")
                
                pass
        pygame.quit()

    def __init__(self, host_name = "localhost", device_path='/dev/input/js0'):
        ''' Initalize class variables '''
        rospy.init_node("wheel_console", log_level=rospy.INFO)
        self.device_path = device_path
        pygame.init()
        self.done = False
        self.init_status = False
        self.init_system = False
        self.init_steering = False
        self.init_brake = False
        self.init_gas = False
        
        self.last_mode = None
        
        self.is_pod_estop = False
        self.is_pod_pbrake =  False
        self.is_pod_dooropen = False

        self.gas = 0
        self.brake = 0
        self.steering = 0
        self.cur_speed = 0
        self.speed_scale = SPEED_SCALE
        self.max_speed = FORWARD_MAX_SPEED #km/h
        self.max_reverse_speed = -REVERSE_MAX_SPEED
        self.estop = 0
        self.is_brake = 0
        self.cur_speed = 0
        self.clock = pygame.time.Clock()
        self.pod_buttons = {"ts": 0, "isBrake":0, "isAuto":0 }

        self.ros_publisher =  rospy.Publisher('/wheel_console', wheel_console, queue_size=1)
        # Initialize the joysticks
        pygame.joystick.init()
        self.joystick_count = pygame.joystick.get_count()
        logger.info("joystick found : " + str(self.joystick_count))
        self.read_joystick()


if __name__ == '__main__':
    DriveForceSteeringInterface()
    



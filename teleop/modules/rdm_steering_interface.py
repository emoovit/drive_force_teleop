#!/usr/bin/env python
""" RDM steering interface
# -*- coding: utf-8 -*-
#
# Author : Balaji Ravichandiran
# Email : balaji@moovita.com
# Department : Moovita, SG
# Last modified: 20-AUG-2018
# version comments - convert to ROS
"""

import rospy
import pygame
import serial
import time
from pprint import pprint
import logging
import os
from ConfigParser import SafeConfigParser
from moovita_message_definitions.msg import wheel_console


FILE_LOCATION = os.path.dirname(os.path.abspath(__file__))

#------------Config Parser---------
config = SafeConfigParser()
config.read(FILE_LOCATION+'/config/config.ini')

FORWARD_MAX_SPEED = config.getfloat('WHEEL_CONSOLE', 'FORWARD_MAX_SPEED')
REVERSE_MAX_SPEED = config.getfloat('WHEEL_CONSOLE', 'REVERSE_MAX_SPEED')
SPEED_SCALE = config.getfloat('WHEEL_CONSOLE', 'SPEED_SCALE')
PUBLISH_FREQ = config.getfloat('WHEEL_CONSOLE', 'PUBLISH_FREQ')
STEERING_RANGE = config.getint('WHEEL_CONSOLE', 'STEERING_RANGE')
LOG_LEVEL = config.get('WHEEL_CONSOLE', 'LOG_LEVEL')


#--------------end of Config -------


class ConnectPythonLoggingToROS(logging.Handler):

    MAP = {
        logging.DEBUG:rospy.logdebug,
        logging.INFO:rospy.loginfo,
        logging.WARNING:rospy.logwarn,
        logging.ERROR:rospy.logerr,
        logging.CRITICAL:rospy.logfatal
    }

    def emit(self, record):
        try:
            self.MAP[record.levelno]("%s: %s" % (record.name, record.msg))
        except KeyError:
            rospy.logerr("unknown log level %s LOG: %s: %s" % (record.levelno, record.name, record.msg))
            

#----------Logging Initalize -------#

#reconnect logging calls which are children of this to the ros log system
logging.getLogger('steering').addHandler(ConnectPythonLoggingToROS())

if not os.path.exists("logs"):
    os.makedirs("logs")
    
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("steering")

# file handler
handler = logging.FileHandler('logs/steering_interface.log')
handler.setLevel(logging.INFO)

# logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)

#----------Logging Initalize -------#

class RdmSteeringInterface:
    

    def read_joystick(self):
        ''' Main loop to read joystick '''
        while (not self.done) and (not rospy.is_shutdown()):
            
            if not os.path.exists(self.device_path):
                time.sleep(0.5)
                try:
                    logger.info("Reconnecting")
                    # Initialize the joysticks
                    pygame.joystick.quit()
                    pygame.joystick.init()
                    self.joystick_count = pygame.joystick.get_count()
                    logger.info("joystick found : " + str(self.joystick_count))
                    self.init_status = False
                except Exception as e:
                    logger.error(e)
                    pygame.joystick.quit()
                    self.init_status = False
                    time.sleep(1)

            else:
                # EVENT PROCESSING STEP
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        self.done = True
                        self.init_status = False
             
                    # Possible joystick actions: JOYAXISMOTION JOYBALLMOTION JOYBUTTONDOWN
                    # JOYBUTTONUP JOYHATMOTION
                    if event.type == pygame.JOYBUTTONDOWN:
                        logger.info("Joystick button pressed.")
                        
                        if int(joystick.get_button(4)) == 1 and int(joystick.get_button(5)) == 1 :
                            self.gas = 0
                            self.estop = 1
                            self.is_brake = 1
                            logger.debug("BOTH PRESS")
                            if self.init_status == False:
                                self.init_status = True
                        elif int(joystick.get_button(4)) == 1 or int(joystick.get_button(5)) == 1 :
                            logger.info("self.brake")
                            self.gas = 0
                            self.estop = 0
                            self.is_brake = 1
                        elif int(joystick.get_button(7)) == 1 or int(joystick.get_button(6)):
                        #elif int(joystick.get_button(7)) == 1:
                            logger.info("self.gas UP")
                            self.gas = self.gas + self.speed_scale
                            if self.gas > self.max_speed:
                                self.gas = self.max_speed
                            self.estop = 0
                            self.is_brake = 0
                        elif False and int(joystick.get_button(6)):
                            logger.info("TEST SPEED")
                            self.gas = 10
                            if self.gas > self.max_speed:
                                self.gas = self.max_speed
                            self.estop = 0
                            self.is_brake = 0
                            
                        elif int(joystick.get_button(0)) == 1:
                            logger.info("self.gas DOWN")
                            self.gas = self.gas - self.speed_scale
                            if self.gas <= self.max_reverse_speed:
                                self.gas = self.max_reverse_speed
                            self.estop = 0
                            self.is_brake = 0


                        #guard from over speeding in begining
                        if self.gas > 2.5 and self.cur_speed < 2.0:
                            self.gas = 2.5
                        
                        if self.gas < -2.5 and self.cur_speed < 2.0:
                            self.gas = -2.5
                        

                    if event.type == pygame.JOYBUTTONUP:
                        logger.debug("Joystick button released.")


                joystick = pygame.joystick.Joystick(0)
                joystick.init()
                self.joystick_count = pygame.joystick.get_count()
                self.steering = int(joystick.get_axis( 0 )*STEERING_RANGE)  # get value from axis 2

                if self.steering < -STEERING_RANGE:
                    self.steering = -STEERING_RANGE

                if self.steering > STEERING_RANGE:
                    self.steering = STEERING_RANGE
                
                _msg = "INIT: %s, GAS: %s, STEERING : %s, ESTOP : %s, JOYSTICKS : %s" %(self.init_status, self.gas, self.steering, self.estop ,  self.joystick_count)
                logger.debug(_msg)

                pub_msg = wheel_console()
                pub_msg.header.stamp = rospy.Time.now()
                pub_msg.header.frame_id = 'moovita'
                
                
                if self.init_status:
                    pub_msg.cmd_steering = self.steering
                    pub_msg.cmd_speed = self.gas

                else:
                    pub_msg.cmd_speed = 0

                pub_msg.is_estop = self.estop
                pub_msg.is_brake = self.is_brake
                pub_msg.is_init = self.init_status

                self.ros_publisher.publish(pub_msg)
                
            time.sleep(PUBLISH_FREQ)
        pygame.quit()

    def __init__(self, host_name = "localhost", device_path='/dev/input/js0'):
        ''' Initalize class variables '''
        rospy.init_node("wheel_console", log_level=rospy.INFO)
        self.device_path = device_path
        pygame.init()
        self.done = False
        self.init_status = False

        self.last_mode = None
        
        self.is_pod_estop = False
        self.is_pod_pbrake =  False
        self.is_pod_dooropen = False

        self.gas = 0
        self.brake = 0
        self.steering = 0
        self.cur_speed = 0
        self.speed_scale = SPEED_SCALE
        self.max_speed = FORWARD_MAX_SPEED #km/h
        self.max_reverse_speed = -REVERSE_MAX_SPEED
        self.estop = 0
        self.is_brake = 0
        self.cur_speed = 0
        self.clock = pygame.time.Clock()
        self.pod_buttons = {"ts": 0, "isBrake":0, "isAuto":0 }

        self.ros_publisher =  rospy.Publisher('/wheel_console', wheel_console, queue_size=1)
        # Initialize the joysticks
        pygame.joystick.init()
        self.joystick_count = pygame.joystick.get_count()
        logger.info("joystick found : " + str(self.joystick_count))
        self.read_joystick()


if __name__ == '__main__':
    RdmSteeringInterface()
    


